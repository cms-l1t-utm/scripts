#include <cereal/cereal.hpp>
#include <cereal/archives/json.hpp>
#include <cereal/types/vector.hpp>

#include <TFile.h>
#include <TTree.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>

#include "menulib.hh"
#include "../include/MenuObject.hh"
#include "../include/MenuCut.hh"
#include "../include/MenuCondition.hh"

#include "L1Trigger/L1TNtuples/interface/L1AnalysisEventDataFormat.h"


bool
l1_singlemu16(TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat>& stage2,
              bool json=false)
{
  bool rc = false;
  auto muon = std::make_shared<MenuCondition::Object>();

  std::string name = "L1_SingleMu16";
  MenuCut::Object cut(MenuObject::Muon, 16.);
  cut.quality = MenuCut::MU_QLTY_SNGL;
  muon->cuts.push_back(cut);

  rc = muon->select(stage2);
  int id = getIdFromName(name);
#ifdef CEREAL_CEREAL_HPP_
  if (json)
  {
    std::ofstream ofs;
    ofs.open(name + ".json", std::ofstream::out);
    cereal::JSONOutputArchive output(ofs);
    muon->print(name, output);
  }
#endif
  if (id >= 0 and getFuncFromId(id)(stage2) != rc) std::cout << "ERROR: " + name << "\n";

  return rc;
}


 
void
sample(TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat>& data,
       bool json=false)
{
  //
  // use methods generated from menu
  //
  std::string name = "L1_SingleTau80er";
  int id = getIdFromName(name);
  if (id >= 0 and getFuncFromId(id)(data)) std::cout << name << "\n";

  name = "L1_DoubleEG_20_18";
  id = getIdFromName(name);
  if (id >= 0 and getFuncFromId(id)(data)) std::cout << name << "\n";

  name = "L1_TripleJet_84_68_48_VBF";
  id = getIdFromName(name);
  if (id >= 0 and getFuncFromId(id)(data)) std::cout << name << "\n";

  name = "L1_QuadMu0";
  id = getIdFromName(name);
  if (id >= 0 and getFuncFromId(id)(data)) std::cout << name << "\n";

  name = "L1_ETM90";
  id = getIdFromName(name);
  if (id >= 0 and getFuncFromId(id)(data)) std::cout << name << "\n";

  name = "L1_Mu3_JetC16_dEta_Max0p4_dPhi_Max0p4";
  id = getIdFromName(name);
  if (id >= 0 and getFuncFromId(id)(data)) std::cout << name << "\n";


  std::ofstream ofs;
  if (json) ofs.open("parameters.json", std::ofstream::out);
  cereal::JSONOutputArchive output(ofs);

  //
  // construct algorithms and serialize them for creating xml file
  //

  //
  // as a function
  l1_singlemu16(data, json);


  //
  // inline definitions

  // single-muon
  auto mu10 = std::make_shared<MenuCondition::Object>();
  {
    std::string name = "L1_SingleMu10";
    MenuCut::Object cut(MenuObject::Muon, 10.);
    cut.eta1.minimum = -2.50;
    cut.eta1.maximum = -2.10;
    cut.eta2.minimum = +2.10;
    cut.eta2.maximum = +2.50;
    cut.phi1.minimum = 0.0;
    cut.phi1.maximum = 3.00;
    cut.phi2.minimum = 3.14;
    cut.phi2.maximum = 6.28;
    cut.quality = MenuCut::MU_QLTY_SNGL;
    mu10->cuts.push_back(cut);
    if (mu10->select(data)) std::cout << name << "\n";
    if (json) mu10->print(name, output);
  }


  // double-muon
  auto mu_10_open = std::make_shared<MenuCondition::Object>();
  {
    std::string name = "L1_DoubleMu_10_Open";
    MenuCut::Object cut(MenuObject::Muon, 10.);
    cut.quality = MenuCut::MU_QLTY_DBLE;
    mu_10_open->cuts.push_back(cut);
    cut.threshold = 0.;
    cut.quality = MenuCut::MU_QLTY_OPEN;
    mu_10_open->cuts.push_back(cut);
    bool rc = mu_10_open->select(data);
    if (rc) std::cout << name << "\n";
    if (json) mu_10_open->print(name, output);
    int id = getIdFromName(name);
    if (id >= 0 and getFuncFromId(id)(data) != rc) std::cout << "ERROR: " + name << "\n";
  }

  // triple-eg
  auto eg_14_10_8 = std::make_shared<MenuCondition::Object>();
  {
    std::string name = "L1_TripleEG_14_10_8";
    MenuCut::Object cut(MenuObject::Egamma, 14.);
    eg_14_10_8->cuts.push_back(cut);
    cut.threshold = 10.;
    eg_14_10_8->cuts.push_back(cut);
    cut.threshold = 8.;
    eg_14_10_8->cuts.push_back(cut);
    bool rc = eg_14_10_8->select(data);
    if (rc) std::cout << name << "\n";
    if (json) eg_14_10_8->print(name, output);
    int id = getIdFromName(name);
    if (id >= 0 and getFuncFromId(id)(data) != rc) std::cout << "ERROR: " + name << "\n";
  }

  // energy sum
  auto etm_90 = std::make_shared<MenuCondition::Object>();
  {
    std::string name = "L1_ETM90";
    MenuCut::Object cut(L1Analysis::kMissingEt, 90.);
    cut.phi1.minimum = 0.0;
    cut.phi1.maximum = 3.00;
    cut.phi2.minimum = 3.14;
    cut.phi2.maximum = 6.28;
    etm_90->cuts.push_back(cut);
    bool rc = etm_90->select(data);
    if (rc) std::cout << name << "\n";
    if (json) etm_90->print(name, output);
    int id = getIdFromName(name);
    if (id >= 0 and getFuncFromId(id)(data) != rc) std::cout << "ERROR: " + name << "\n";
  }

  // correlations
  auto etm70_jet60_dphi_min0p4 = std::make_shared<MenuCondition::Correlation>();
  {
    std::string name = "L1_ETM70_Jet60_dPhi_Min0p4";
    MenuCut::Correlation corrCut;
    corrCut.deltaPhi.minimum = +4.0000000000000002E-01;
    corrCut.deltaPhi.maximum = +3.1499999999999999E+00;
    etm70_jet60_dphi_min0p4->objectCut0 = MenuCut::Object(L1Analysis::kMissingEt, 70.);
    etm70_jet60_dphi_min0p4->objectCut1 = MenuCut::Object(MenuObject::Jet, 60.);
    etm70_jet60_dphi_min0p4->correlationCut = corrCut;
    bool rc = etm70_jet60_dphi_min0p4->select(data);
    if (rc) std::cout << name << "\n";
    if (json) etm70_jet60_dphi_min0p4->print(name, output);
    int id = getIdFromName(name);
    if (id >= 0 and getFuncFromId(id)(data) != rc) std::cout << "ERROR: " + name << "\n";
  }

  // composite triggers
  auto AND = std::make_shared<MenuCondition::Operator>(MenuCondition::Operator::AND);
  auto OR = std::make_shared<MenuCondition::Operator>(MenuCondition::Operator::OR);

  auto mu5 = std::make_shared<MenuCondition::Object>();
  auto eg15 = std::make_shared<MenuCondition::Object>();
  {
    std::string name = "L1_Mu5_EG15";
    MenuCut::Object muon_cut(MenuObject::Muon, 5.);
    muon_cut.quality = MenuCut::MU_QLTY_SNGL;
    mu5->cuts.push_back(muon_cut);

    MenuCut::Object eg_cut(MenuObject::Egamma, 15.);
    eg15->cuts.push_back(eg_cut);

    MenuCondition::Composite c;
    // push in RPN order
    c.operands.push_back(mu5);
    c.operands.push_back(eg15);
    c.operands.push_back(AND);

    bool rc = c.select(data);
    if (rc) std::cout << name << "\n";
    if (json) c.print(name, output);

    int id = getIdFromName(name);
    if (id >= 0 and getFuncFromId(id)(data) != rc) std::cout << "ERROR: " + name << "\n";
  }

  auto jet_84c_68c_48c = std::make_shared<MenuCondition::Object>();
  auto jet_84c_68c = std::make_shared<MenuCondition::Object>();
  auto jet_48f = std::make_shared<MenuCondition::Object>();
  auto jet_84c_48c = std::make_shared<MenuCondition::Object>();
  auto jet_68f = std::make_shared<MenuCondition::Object>();
  auto jet_68c_48c = std::make_shared<MenuCondition::Object>();
  auto jet_84f = std::make_shared<MenuCondition::Object>();
  {
    std::string name = "L1_TripleJet_84_68_48_VBF";
    MenuCut::Object jet84c(MenuObject::Jet, 84.);
    jet84c.eta1.minimum = -3.0014999999999996E+00;
    jet84c.eta1.maximum = +3.0014999999999996E+00;

    MenuCut::Object jet84f(MenuObject::Jet, 84.);
    jet84f.eta1.minimum = -5.0000000000000000E+00;
    jet84f.eta1.maximum = -3.0014999999999996E+00;
    jet84f.eta2.minimum = +3.0014999999999996E+00;
    jet84f.eta2.maximum = +5.0000000000000000E+00;

    MenuCut::Object jet68c(MenuObject::Jet, 68.);
    jet68c.eta1.minimum = -3.0014999999999996E+00;
    jet68c.eta1.maximum = +3.0014999999999996E+00;

    MenuCut::Object jet68f(MenuObject::Jet, 68.);
    jet68f.eta1.minimum = -5.0000000000000000E+00;
    jet68f.eta1.maximum = -3.0014999999999996E+00;
    jet68f.eta2.minimum = +3.0014999999999996E+00;
    jet68f.eta2.maximum = +5.0000000000000000E+00;

    MenuCut::Object jet48c(MenuObject::Jet, 48.);
    jet48c.eta1.minimum = -3.0014999999999996E+00;
    jet48c.eta1.maximum = +3.0014999999999996E+00;

    MenuCut::Object jet48f(MenuObject::Jet, 48.);
    jet48f.eta1.minimum = -5.0000000000000000E+00;
    jet48f.eta1.maximum = -3.0014999999999996E+00;
    jet48f.eta2.minimum = +3.0014999999999996E+00;
    jet48f.eta2.maximum = +5.0000000000000000E+00;

    jet_84c_68c_48c->cuts.push_back(jet84c);
    jet_84c_68c_48c->cuts.push_back(jet68c);
    jet_84c_68c_48c->cuts.push_back(jet48c);

    jet_84c_68c->cuts.push_back(jet84c);
    jet_84c_68c->cuts.push_back(jet68c);

    jet_48f->cuts.push_back(jet48f);

    jet_84c_48c->cuts.push_back(jet84c);
    jet_84c_48c->cuts.push_back(jet48c);

    jet_68f->cuts.push_back(jet68f);

    jet_68c_48c->cuts.push_back(jet68c);
    jet_68c_48c->cuts.push_back(jet48c);

    jet_84f->cuts.push_back(jet84f);

    MenuCondition::Composite c;
    // push in RPN order
    c.operands.push_back(jet_84c_68c_48c); // 84c-68c-48c
    c.operands.push_back(jet_84c_68c); // 84c-68c
    c.operands.push_back(jet_48f); // 48f
    c.operands.push_back(AND);
    c.operands.push_back(OR);
    c.operands.push_back(jet_84c_48c); // 84c-48c
    c.operands.push_back(jet_68f); // 68f
    c.operands.push_back(AND);
    c.operands.push_back(OR);
    c.operands.push_back(jet_68c_48c); // 68c-48c
    c.operands.push_back(jet_84f); // 48f
    c.operands.push_back(AND);
    c.operands.push_back(OR);

    bool rc = c.select(data);
    if (rc) std::cout << name << "\n";
    if (json) c.print(name, output);

    int id = getIdFromName(name);
    if (id >= 0 and getFuncFromId(id)(data) != rc) std::cout << "ERROR: " + name << "\n";
  }

  return;
}


int
main()
{
  TFile* file = new TFile("L1Ntuple.root");
  TTree* tree;
  file->GetObject("l1UpgradeTree/L1UpgradeTree", tree);

  TTreeReader reader;
  TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat> stage2 = {reader, "L1Upgrade"};
  reader.SetTree(tree);

  TTree* event_tree;
  file->GetObject("l1EventTree/L1EventTree", event_tree);
  TTreeReader event_reader;
  TTreeReaderValue<L1Analysis::L1AnalysisEventDataFormat> event = {event_reader, "Event"};
  event_reader.SetTree(event_tree);

  std::cout << "example:\n";
  size_t nn = 0;
  while (reader.Next())
  {
    event_reader.Next();
    std::cout << " ***** " << ++nn << " run: " << event->run << " " << " event: " << event->event << " " << "ls: " << event->lumi << "\n";

    sample(stage2, nn==1);
    if (nn == 100) break;
  }
}
