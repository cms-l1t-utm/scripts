#include "MenuObject.hh"
#include "MenuCut.hh"


void
MenuObject::clear()
{
  type = Undef;
  bx = 0;
  et = 0.;
  eta = 0.;
  phi = 0.;
  quality = 0;
  isolation = 0;
  charge = 0;
  iet = 0;
  ieta = 0;
  iphi = 0;
}


void
MenuObject::set(TTreeReaderValue<L1Analysis::L1AnalysisL1UpgradeDataFormat>& data,
                const int index)
{
  if (type == Muon)
  {
    bx = data->muonBx.at(index);
    et = data->muonEt.at(index);
    eta = data->muonEta.at(index);
    phi = data->muonPhi.at(index);
    quality = data->muonQual.at(index);
    isolation = data->muonIso.at(index);
    charge = data->muonChg.at(index);
    iet = data->muonIEt.at(index);
    ieta = data->muonIEta.at(index);
    iphi = data->muonIPhi.at(index);
  }
  else if (type == Egamma)
  {
    bx = data->egBx.at(index);
    et = data->egEt.at(index);
    eta = data->egEta.at(index);
    phi = data->egPhi.at(index);
    isolation = data->egIso.at(index);
    iet = data->egIEt.at(index);
    ieta = data->egIEta.at(index);
    iphi = data->egIPhi.at(index);
  }
  else if (type == Jet)
  {
    bx = data->jetBx.at(index);
    et = data->jetEt.at(index);
    eta = data->jetEta.at(index);
    phi = data->jetPhi.at(index);
    iet = data->jetIEt.at(index);
    ieta = data->jetIEta.at(index);
    iphi = data->jetIPhi.at(index);
  }
  else if (type == Tau)
  {
    bx = data->tauBx.at(index);
    et = data->tauEt.at(index);
    eta = data->tauEta.at(index);
    phi = data->tauPhi.at(index);
    isolation = data->tauIso.at(index);
    iet = data->tauIEt.at(index);
    ieta = data->tauIEta.at(index);
    iphi = data->tauIPhi.at(index);
  }
  else if (type == L1Analysis::kTotalEt or
           type == L1Analysis::kTotalHt or
           type == L1Analysis::kMissingEt or
           type == L1Analysis::kMissingHt or
           type == L1Analysis::kMissingEtHF or
           type == L1Analysis::kMinBiasHFP0 or
           type == L1Analysis::kMinBiasHFM0 or
           type == L1Analysis::kMinBiasHFP1 or
           type == L1Analysis::kMinBiasHFM1 or
           type == L1Analysis::kTotalEtEm or
           type == L1Analysis::kTowerCount)
  {
    bx = data->sumBx.at(index);
    et = data->sumEt.at(index);
    phi = data->sumPhi.at(index);
    iet = data->sumIEt.at(index);
    iphi = data->sumIPhi.at(index);
  }
  else
  {
    throw std::runtime_error("MenuObject::set: unknown type");
  }
}


bool
MenuObject::select(MenuCut::Object& cut)
{
  if (not (type == cut.type)) return false;
  if (not (bx == cut.bx)) return false;

  cut.setEmulationCut();

  if (not (iet >= cut.ithreshold)) return false;

  if (cut.ieta1.minimum != cut.ieta1.maximum)
  {
    bool etaWindow = ((cut.ieta1.minimum <= ieta) and (ieta < cut.ieta1.maximum));
    if (cut.ieta2.minimum != cut.ieta2.maximum)
    {
      etaWindow |= ((cut.ieta2.minimum <= ieta) and (ieta < cut.ieta2.maximum));
    }
    if (not etaWindow) return false;
  }

  if (cut.iphi1.minimum != cut.iphi1.maximum)
  {
    bool phiWindow = ((cut.iphi1.minimum <= iphi) and (iphi < cut.iphi1.maximum));
    if (cut.iphi2.minimum != cut.iphi2.maximum)
    {
      phiWindow |= ((cut.iphi2.minimum <= iphi) and (iphi < cut.iphi2.maximum));
    }
    if (not phiWindow) return false;
  }

  if (cut.quality != MenuCut::NO_QUALITY)
  {
    if (not ((cut.quality >> quality) & 1)) return false;
  }

  if (cut.isolation != MenuCut::NO_ISOLATION)
  {
    if (not ((cut.isolation >> isolation) & 1)) return false;
  }

  if (cut.charge)
  {
    if (not (cut.charge == charge)) return false;
  }

  return true;
}

// eof
